<%@ page import="cl.azurian.yv.bl.restfull.client.*, cl.azurian.yv.bt.entity.*, java.util.List" %>
<html>
<head>
<title>Mostrar Listado de Empleados</title>
</head>
<body>
<%
    GetLaborBySkillClient cliente= new GetLaborBySkillClient();
	List<LaborBySkill> lstLaborBySkill = cliente.getLstLaborBySkill();
	
    String text="";
    text="Listado de Empleados: <br>";
    text=text+"<br>";
 
    if (lstLaborBySkill!=null){
		for (LaborBySkill lbBySkill : lstLaborBySkill) {
			text = text+"<br>";
			text = text+"Registro: "+lbBySkill.getLastname()+","+
											lbBySkill.getFirstname()+","+
											lbBySkill.getIdcard()+","+
											lbBySkill.getEmail()+","+
											lbBySkill.getPhone()+","+
											lbBySkill.getLaborposition()+","+
											lbBySkill.getArea()+","+
											lbBySkill.getDescription();	
		}
    }else{
        text=text+"No se obtuvieron resultados para la consulta seleccionada.";
    }
%>
<%= text %>
</body>
</html>