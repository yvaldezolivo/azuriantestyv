<%@ page import="cl.azurian.yv.bl.restfull.client.*, cl.azurian.yv.bt.entity.*, java.util.List" %>
<html>
<head>
<title>Mostrar Listado de Empleados</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="resources/bootstrap-table-pagination.js"></script>
<script src="resources/pagination.js"></script>
</head>
<body>
<%
    GetLaborBySkillClient cliente= new GetLaborBySkillClient();
	List<LaborBySkill> lstLaborBySkill = cliente.getLstLaborBySkill();
	
    String text="";
    text="<br><h1>Listado de Empleados: <h1><br>";
    
    if (lstLaborBySkill!=null){
	    text=text+"<div class=\"table-responsive\">";
	    text=text+"<table class=\"table table-hover table-bordered\">";
	    text=text+"<thead>";
	    text=text+"<tr>";
	    text=text+"<th>Apellidos</th>";
	    text=text+"<th>Nombres</th>";
	    text=text+"<th>Rut</th>";
	    text=text+"<th>Correo</th>";
	    text=text+"<th>Telefono</th>";
	    text=text+"<th>Posicion</th>";
	    text=text+"<th>Area</th>";
	    text=text+"<th>Descripcion</th>";
	    text=text+"</tr>";
	    text=text+"</thead>";
	    text=text+"<tbody id=\"laborBySkill\">";
	    
	    
	    for (LaborBySkill lbBySkill : lstLaborBySkill) {
	    	text=text+"<tr>";
		    	text=text+"<td>"+lbBySkill.getLastname()+"</td>";
		    	text=text+"<td>"+lbBySkill.getFirstname()+"</td>";
		    	text=text+"<td>"+lbBySkill.getIdcard()+"</td>";
		    	text=text+"<td>"+lbBySkill.getEmail()+"</td>";
		    	text=text+"<td>"+lbBySkill.getPhone()+"</td>";
		    	text=text+"<td>"+lbBySkill.getLaborposition()+"</td>";
		    	text=text+"<td>"+lbBySkill.getArea()+"</td>";
		    	text=text+"<td>"+lbBySkill.getDescription()+"</td>";
	    	text=text+"</tr>";
		}
	    
	    text=text+"</tbody>";
	    text=text+"</table>";
	    text=text+"</div>";
	    
	    text=text+"<div class=\"col-md-12 text-center\">";
	    text=text+"<ul class=\"pagination pagination-lg pager\" id=\"laborBySkill_page\"></ul>";
	    text=text+"</div>";
    }else{
        text=text+"No se obtuvieron resultados para la consulta seleccionada.";
    }
%>
<%= text %>
</body>
</html>