package cl.azurian.yv.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * 	 Utilitario EntityManagerUtil
 *   Retorna el EntityManager de la PU YValdezPU
 *   @author Yhioshira Valdez
 *   @version 1.0
 *   @since 2019-01-23 
 */
public class EntityManagerUtil {
	/**
	 * 	 Metodo getAll()
	 *   Obtiene todos los registros de una entidad
	 *   @author Yhioshira Valdez
	 *   @version 1.0
	 *   @since 2019-01-23 
	 */
    public static EntityManager getEntityManager(String pu){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(pu);
        EntityManager manager = factory.createEntityManager();
        return manager;
    }
}