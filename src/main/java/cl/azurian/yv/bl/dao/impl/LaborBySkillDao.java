package cl.azurian.yv.bl.dao.impl;

import java.util.List;
import javax.persistence.Query;
import cl.azurian.yv.bl.dao.InterfaceDao;
import cl.azurian.yv.bt.entity.LaborBySkill;
import cl.azurian.yv.util.EntityManagerUtil;

/**
 * 	 DAO LaborBySkillDao
 *   Implementa la funcionalidad de la interfaz InterfaceDao para la entidad LaborBySkill
 *   @author Yhioshira Valdez
 *   @version 1.0
 *   @since 2019-01-23 
 */
public class LaborBySkillDao implements InterfaceDao<LaborBySkill>{
	
	/**
	 * 	 Metodo getAll()
	 *   Sobreescribe el metodo getAll() de la interfaz para definir la funcionalidad requerida para la entidad LaborBySkill
	 *   @author Yhioshira Valdez
	 *   @version 1.0
	 *   @since 2019-01-23 
	 *   @return Listado de registros de entidad LaborBySkill
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<LaborBySkill> getAll() {
		Query query = EntityManagerUtil.getEntityManager("YValdezPU").createQuery("SELECT t FROM LaborBySkill t");
		return query.getResultList();
	}
	
	public static void main(String[] args) {
		System.out.println("Probando la conexion con la DB...");
		LaborBySkillDao laborSkillDao = new LaborBySkillDao();
		List<LaborBySkill> lstLaborBySkill = laborSkillDao.getAll();
		for (int i = 1; i<lstLaborBySkill.size(); i++) {
			System.out.println("Registro: "+lstLaborBySkill.get(i).getLastname());
		}
	}
}