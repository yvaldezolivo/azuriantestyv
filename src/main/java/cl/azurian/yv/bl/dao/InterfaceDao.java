package cl.azurian.yv.bl.dao;

import java.util.List;

/**
 * 	 Interface LaborBySkillDAO
 *   Debido a que el ejercicio implica solo la lectura de registros solo se incluye el metodo getAll,
 *   aqui deberian definirse los metodos que correspondan a la interaccion esperada con los ODB
 *   @author Yhioshira Valdez
 *   @version 1.0
 *   @since 2019-01-23
 *   @param T Entidad
 */
public interface InterfaceDao<T> {
	
	/**
	 * 	 Definicion del metodo getAll() que se encarga de obtener todos los registros de una entidad
	 *   @author Yhioshira Valdez
	 *   @since 2019-01-23
	 *   @return Listado de registros de entidad
	 */
	List<T> getAll();
}