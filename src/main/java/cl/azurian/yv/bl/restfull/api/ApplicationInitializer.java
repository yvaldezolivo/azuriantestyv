package cl.azurian.yv.bl.restfull.api;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * 	 Clase ApplicationInitializer
 *   Implementa el API de Inicializacion para WS Rest
 *   @author Yhioshira Valdez
 *   @version 1.0
 *   @since 2019-01-23 
 */
public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { ApplicationConfiguration.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() { 
		return null; 
	}

	@Override
	protected String[] getServletMappings() { 
		return new String[] { "/rest/*" };
	}
}