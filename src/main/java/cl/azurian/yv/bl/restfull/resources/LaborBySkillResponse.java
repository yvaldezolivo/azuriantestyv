package cl.azurian.yv.bl.restfull.resources;

/**
 * 	 Clase LaborBySkillResponse
 *   Modela el response para el WS Rest [server]:[port]/AzurianTestYValdez/rest/getLabor
 *   @author Yhioshira Valdez
 *   @version 1.0
 *   @since 2019-01-23 
 */
public class LaborBySkillResponse {
	private String resultado;
	
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
}