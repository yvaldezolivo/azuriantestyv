package cl.azurian.yv.bl.restfull.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import cl.azurian.yv.bl.dao.impl.LaborBySkillDao;
import cl.azurian.yv.bl.restfull.resources.LaborBySkillResponse;
import cl.azurian.yv.bt.entity.LaborBySkill;

/**
 * 	 Clase Controlador LaborBySkillController
 *   Implementa la funcionalidad del WS Rest [server]:[port]/AzurianTestYValdez/rest/getLabor
 *   @author Yhioshira Valdez
 *   @version 1.0
 *   @since 2019-01-23 
 */
@RestController
@RequestMapping("/getLabor")
public class LaborBySkillController {
	/**
	 * 	 Metodo getLaborBySkill()
	 *   Implementa la funcionalidad del metodo /getLaborBySkill del WS Rest [server]:[port]/AzurianTestYValdez/rest/getLabor
	 *   @author Yhioshira Valdez
	 *   @version 1.0
	 *   @since 2019-01-23 
	 *   @return Response del WS Rest [server]:[port]/AzurianTestYValdez/rest/getLabor
	 */
	@RequestMapping(value = "/getLaborBySkill", method = RequestMethod.GET, produces = "application/json")
	public LaborBySkillResponse getLaborBySkill() {
		LaborBySkillResponse response = new LaborBySkillResponse();
		LaborBySkillDao laborBySkillDao = new LaborBySkillDao();
		List<LaborBySkill> lstLaborbySkill = laborBySkillDao.getAll();
		try {
			Gson gson = new Gson();
			Type listType = new TypeToken<ArrayList<LaborBySkill>>(){}.getType();
			String resultado = gson.toJson(lstLaborbySkill, listType);
			response.setResultado(resultado);
		} catch (Exception exc) {
			System.out.println("Exception: " + exc);
		}
		return response;
	}
}