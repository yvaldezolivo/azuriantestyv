package cl.azurian.yv.bl.restfull.client;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import cl.azurian.yv.bl.restfull.resources.LaborBySkillResponse;
import cl.azurian.yv.bt.entity.LaborBySkill;

public class GetLaborBySkillClient {
	private String getLaborBySkillCl() {
		RestTemplate restTemplate = new RestTemplate();
        String uri = "http://localhost:8080/AzurianTestYValdez/rest/getLabor/getLaborBySkill";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("resultado", headers);
        ResponseEntity<String> laborBySkill = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        
        if (laborBySkill!=null) return laborBySkill.getBody();
        else return null;
	}
	
	private LaborBySkillResponse serializarResponse(String response) {
		Gson gson = new Gson();
		System.out.println("Resultado: "+response);
		LaborBySkillResponse laborBySkill = new LaborBySkillResponse();
		Type listType = new TypeToken<LaborBySkillResponse>(){}.getType();
		laborBySkill = gson.fromJson(response, listType);
		return laborBySkill;
	}
	
	public List<LaborBySkill> getLstLaborBySkill() {
		Gson gson = new Gson();
		List<LaborBySkill> lstResultados = new ArrayList<LaborBySkill>();
		GetLaborBySkillClient cliente = new GetLaborBySkillClient();
		String resultado = cliente.getLaborBySkillCl();
		if (resultado!=null) {
			LaborBySkillResponse response = new LaborBySkillResponse();
			response = cliente.serializarResponse(resultado);
			if (response!=null) {
				Type listType = new TypeToken<List<LaborBySkill>>(){}.getType();
				lstResultados = gson.fromJson(response.getResultado(), listType);
			}
		}
		return lstResultados;
	}
	
	public static void main(String[] args) {
		GetLaborBySkillClient getLaborBySkill = new GetLaborBySkillClient();
		List<LaborBySkill> lstLaborBySkill = getLaborBySkill.getLstLaborBySkill();
		for (LaborBySkill lbBySkill : lstLaborBySkill) {
			System.out.println("Registro: "+lbBySkill.getLastname()+","+
											lbBySkill.getFirstname()+","+
											lbBySkill.getIdcard()+","+
											lbBySkill.getEmail()+","+
											lbBySkill.getPhone()+","+
											lbBySkill.getLaborposition()+","+
											lbBySkill.getArea()+","+
											lbBySkill.getDescription());	
		}
	}
}