package cl.azurian.yv.bl.restfull.api;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * 	 Clase ApplicationConfiguration
 *   Implementa el API de configuracion para WS Rest
 *   @author Yhioshira Valdez
 *   @version 1.0
 *   @since 2019-01-23 
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "cl.azurian.yv.bl.restfull.controllers")
public class ApplicationConfiguration {

}