package cl.azurian.yv.bt.entity;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * 	 Entidad LaborBySkillID
 *   Modela el id compuesto del ODB laborbyskill
 *   @author Yhioshira Valdez
 *   @version 1.0
 *   @since 2019-01-23 
 */
@Embeddable
public class LaborBySkillID implements Serializable{
	private static final long serialVersionUID = 1L;
	private int idperson;
	private int idskill;
	
	public int getIdperson() {
		return idperson;
	}
	
	public void setIdperson(int idperson) {
		this.idperson = idperson;
	}

	public int getIdskill() {
		return idskill;
	}

	public void setIdskill(int idskill) {
		this.idskill = idskill;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idperson;
		result = prime * result + idskill;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LaborBySkillID other = (LaborBySkillID) obj;
		if (idperson != other.idperson)
			return false;
		if (idskill != other.idskill)
			return false;
		return true;
	}
}