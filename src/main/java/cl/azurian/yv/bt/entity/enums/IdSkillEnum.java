package cl.azurian.yv.bt.entity.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 	 Enumerado IdSkillEnum
 *   Contiene los valores esperados en la columna idskill del ODB laborbyskill
 *   @author yvaldez
 *   @version 1.0
 *   @since 2019-01-23 
 */
public enum IdSkillEnum {
	PROI(1), 
	PROII(2), 
	PROIII(3), 
	PROIV(4), 
	ANAI(5), 
	ANAII(6), 
	ANAIII(7), 
	ANAIV(8), 
	GERENCIA(9);
	
	private final int idSkill;
	private static final Map<Integer, IdSkillEnum> MAP = new HashMap<>();
	
	IdSkillEnum(int idSkill) {
        this.idSkill = idSkill;
    }
	
	public int getIdSkill() {
        return this.idSkill;
    }
	
	public static IdSkillEnum fromId(int idSkill){
        return MAP.get(idSkill);
    }
	
    static{
        for(IdSkillEnum n : values()){
            MAP.put(n.getIdSkill(), n);
        }
    }
}