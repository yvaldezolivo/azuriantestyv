package cl.azurian.yv.bt.entity;
 
import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import cl.azurian.yv.bt.entity.enums.IdSkillEnum;

/**
 * 	 Entidad LaborBySkill
 *   Modela el ODB laborbyskill
 *   @author Yhioshira Valdez
 *   @version 1.0
 *   @since 2019-01-23 
 */
@Entity
@Table(name = "LABORBYSKILL", schema = "azurian")
public class LaborBySkill implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId 
    LaborBySkillID id;
    private String lastname;
    private String firstname;
    private String idcard;
    private String email;
    private String phone;
    private String laborposition;
    private String area;
    private String description;
    
    /**
     * Constructor con todos los argumentos
     * @author Yhioshira Valdez
     * @since 2019-01-23 
     * @param lastname Apellidos del empleado
     * @param firstname Nombres del empleado
     * @param idcard Documento de identidad del empleado
     * @param email Correo del empleado
     * @param phone Telefono del empleado
     * @param laborposition Posicion del empleado en la org
     * @param area Area del empleado en la org
     * @param description Descripcion del area del empleado
     * @param idskill ID del skill
     */
    public LaborBySkill(String lastname, String firstname, String idcard, String email, String phone, String laborposition, String area, String description, IdSkillEnum idskill){
    	this.lastname = lastname;
    	this.firstname = firstname;
    	this.idcard = idcard;
    	this.email = email;
    	this.phone = phone;
    	this.laborposition = laborposition;
    	this.area = area;
    	this.description = description;
	}
    
    /**
     * Constructor por defecto
     * @author Yhioshira Valdez
     * @since 2019-01-23 
     */
    public LaborBySkill() {}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLaborposition() {
		return laborposition;
	}

	public void setLaborposition(String laborposition) {
		this.laborposition = laborposition;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}