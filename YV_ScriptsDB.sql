DROP TABLE person;
DROP TABLE labor;
DROP TABLE skills;
DROP TABLE skillLabor;

CREATE TABLE person
(   idPerson    int AUTO_INCREMENT,
    firstName   varchar(40),
    lastName    varchar(40),
    idCard		varchar(20),
    address		varchar(200),
    phone   	varchar(20),
    email       varchar(30),
    dateBird	date,
    createdBy   varchar(15),
    createdDate	datetime DEFAULT NOW(),
    modifyBy	varchar(15),
    modifyDate	datetime DEFAULT NOW(),
    PRIMARY KEY (idPerson),
    INDEX (lastname),
	INDEX (idCard)
);


INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Ana Maria', 'Gonzalez Moncada', '28658987-3', 'Vicuña Mackena 1275, Ñuble, Region Metropolitana, Santiago de Chile, Chile', '958583226', 'amariamoncada@gmail.com', '1984-08-01','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Luis Enrique', 'Castro Davila', '25987461-7', 'Vicuña Mackena 1275, Ñuble, Region Metropolitana, Santiago de Chile, Chile', '958583226', 'lenriquec@gmail.com', '1982-05-15','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Miguel Angel', 'Ocaña Puentes', '26987254-2', 'Moneda 13256, Santiago, Region Metropolitana, Santiago de Chile, Chile', '968520147', 'mangeloca@gmail.com', '1983-11-11','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Clarisa Nohemi', 'Palacios Santander', '27985214-4', 'Holanda 18775, Providencia, Region Metropolitana, Santiago de Chile, Chile', '93647863', 'clarisa.palacios@gmail.com', '1982-10-28','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Ana Katherine', 'Alcantara Reveron', '12587962-3', 'Apoquindo 5488, Las Condes, Region Metropolitana, Santiago de Chile, Chile', '982647003', 'anakareveron@gmail.com', '1984-06-10','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Jose Leonardo', 'Campos Sanchez', '18957356-6', 'Florida 6588, Florida, Region Metropolitana, Santiago de Chile, Chile', '901446963', 'joselcampos@gmail.com', '1981-02-03','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Luis Daniel', 'Rodriguez Posada', '15879501-4', 'Francisco Bilbao 1587, Santiago, Region Metropolitana, Santiago de Chile, Chile', '985360207', 'ldanielrodriguez@gmail.com', '1980-12-07','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Diego Alonso', 'Russian Girardot', '16875014-2', 'Eliodoro Yañez 1398, Providencia, Region Metropolitana, Santiago de Chile, Chile', '936589841', 'dalonsorussian@gmail.com', '1982-07-04','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Greisy Milena', 'Avila Montoya', '25879684-K', 'Nueva Providencia 98771, Providencia, Region Metropolitana, Santiago de Chile, Chile', '963871403', 'greisymavila@gmail.com', '1984-11-25','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Astrid Carolina', 'Luna Mendez', '15877902-6', 'Matucana 1021, Estacion Central, Region Metropolitana, Santiago de Chile, Chile', '934869758', 'astrisclunam@gmail.com', '1983-03-17','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Ana Paola', 'Quezada Barrientos', '16987725-8', 'Av Portugal 1987, Santiago, Region Metropolitana, Santiago de Chile, Chile', '987206398', 'anapquezada@gmail.com', '1981-01-14','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Carlos Alfonzo', 'Olivo Quintana', '19685001-9', 'Merced 369, Santiago, Region Metropolitana, Santiago de Chile, Chile', '965880241', 'calonsoolivoq@gmail.com', '1980-04-27','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Juan Carlos', 'Cepeda Quintero', '24857637-K', 'Willie Arthur 2015, Providencia, Region Metropolitana, Santiago de Chile, Chile', '987552158', 'jcarloscepedaqu@gmail.com', '1979-08-13','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Miriam Carolina', 'Mijares Suarez', '24744691-5', 'José Manuel Infante 2937, Ñuñoa, Region Metropolitana, Santiago de Chile, Chile', '987563221', 'miriamcmijares987@gmail.com', '1978-05-09','yvaldez', now(), 'yvaldez', now());

INSERT INTO person (firstName, lastName, idCard, address, phone, email, dateBird, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('Edward Oscar', 'Plaza Navarro', '15987302-7', 'Fernández 11998, Santiago, Region Metropolitana, Santiago de Chile, Chile', '936578772', 'edwardopnavarro77@gmail.com', '1981-09-11','yvaldez', now(), 'yvaldez', now());

CREATE TABLE labor
(   idLabor     	int AUTO_INCREMENT,
    idPerson   		int,
    laborPosition  	varchar(40),
    initDate		datetime,
    finishDate   	datetime,
    laborStatus     char,
    createdBy   	varchar(15),
    createdDate		datetime DEFAULT NOW(),
    modifyBy		varchar(15),
    modifyDate		datetime DEFAULT NOW(),
    PRIMARY KEY (idLabor),
    FOREIGN KEY (idPerson) REFERENCES person(idPerson),
    INDEX (laborPosition),
    INDEX (laborStatus)
);

INSERT INTO labor (idPerson, laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (1, 'Programador', '2018-01-02', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (2, 'Programador', '2018-01-10', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (3, 'Programador', '2018-02-25', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (4, 'Analista', '2018-03-15', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, finishDate, createdBy, createdDate, modifyBy, modifyDate)
VALUES (5, 'Analista', '2018-02-18', '2018-03-25', 'I', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (6, 'Programador', '2018-03-11', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (7, 'Gerente', '2018-01-15', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (8, 'Analista', '2018-01-02', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, finishDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (9, 'Programador', '2018-02-13', '2018-04-13', 'I', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (10, 'Programador', '2018-02-24', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (11, 'Analista', '2018-03-01', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, finishDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (12, 'Programador', '2018-03-19', '2018-04-21', 'I', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (13, 'Analista', '2018-04-19', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (14, 'Programador', '2018-05-14', 'A', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO labor (laborPosition, initDate, laborStatus, createdBy, createdDate, modifyBy, modifyDate)
VALUES (15, 'Analista', '2018-03-02', 'A', 'yvaldez', now(), 'yvaldez', now());

CREATE TABLE skills
(	idSkill			int AUTO_INCREMENT,
	code			varchar(20),
    description		varchar(255),
    area			varchar(30),
    createdBy   	varchar(15),
    createdDate		datetime DEFAULT NOW(),
    modifyBy		varchar(15),
    modifyDate		datetime DEFAULT NOW(),
    PRIMARY KEY (idSkill),
	UNIQUE (code),
    INDEX (area)
);

INSERT INTO skills (code, description, area, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('PROI', 'Programador I', 'TI', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skills (code, description, area, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('PROII', 'Programador II', 'TI', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skills (code, description, area, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('PROIII', 'Programador III', 'TI', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skills (code, description, area, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('PROIV', 'Programador IV', 'TI', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skills (code, description, area, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('ANAI', 'Analista I', 'TI', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skills (code, description, area, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('ANAII', 'Analista II', 'TI', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skills (code, description, area, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('ANAIII', 'Analista III', 'TI', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skills (code, description, area, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('ANAIV', 'Analista IV', 'TI', 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skills (code, description, area, createdBy, createdDate, modifyBy, modifyDate)
VALUES ('GERENCIA', 'gerente de Area', 'TI', 'yvaldez', now(), 'yvaldez', now());

CREATE TABLE skillLabor
(	idLabor			int,	
	idSkill			int,
    createdBy   	varchar(15),
    createdDate		datetime DEFAULT NOW(),
    modifyBy		varchar(15),
    modifyDate		datetime DEFAULT NOW(),
    CONSTRAINT skill_Labor UNIQUE (idLabor,idSkill),
    FOREIGN KEY (idLabor) REFERENCES labor(idLabor),
    FOREIGN KEY (idSkill) REFERENCES skills(idSkill)
);

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (1, 1, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (2, 2, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (3, 2, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (4, 5, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (5, 6, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (6, 3, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (7, 9, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (8, 5, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (9, 3, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (10, 2, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (11, 7, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (12, 1, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (13, 8, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (14, 1, 'yvaldez', now(), 'yvaldez', now());

INSERT INTO skillLabor (idLabor, idSkill, createdBy, createdDate, modifyBy, modifyDate)
VALUES (15, 8, 'yvaldez', now(), 'yvaldez', now());


CREATE OR REPLACE VIEW laborBySkill AS
select 
ps.idperson,
ps.lastname, 
ps.firstname, 
ps.idcard, 
ps.email, 
ps.phone, 
lb.laborposition, 
sk.area, 
sk.description,
sk.idskill
from person ps, labor lb, skills sk, skillLabor sl
where ps.idPerson = lb.idPerson 
and sl.idLabor = lb.idLabor
and sk.idSkill = sl.idSkill
and lb.laborStatus = 'A'
order by ps.lastname;